package com.kou.leetcodedemo.easy;

public class Application7 {

    public static void main(String[] args) {
        Application7 app = new Application7();
        int result = app.reverse(-2147483647);
        System.out.println(result);

        int a = app.myAtoi(" ");
        System.out.println(a);
    }

    public int reverse(int x) {
        int y = x >= 0 ? x : (x == Integer.MIN_VALUE ? 0 : -x);
        String str = Integer.toString(y);
        str = reverseStr(str);
        String MAX_VALUE_STR = Integer.toString(Integer.MAX_VALUE);
        if (str.length() == MAX_VALUE_STR.length() && str.compareTo(MAX_VALUE_STR) >= 0) {
            y = 0;
        } else {
            y = Integer.parseInt(str);
        }
        return x >= 0 ? y : -y;
    }

    public String reverseStr(String str) {
        char[] chars = str.toCharArray();
        chars = reverseArray(chars);
        str = String.valueOf(chars);
        return str;
    }

    public char[] reverseArray(char[] chars) {
        for (int i = 0; i < chars.length / 2; i++) {
            char tmp = chars[i];
            chars[i] = chars[chars.length - 1 - i];
            chars[chars.length - 1 - i] = tmp;
        }
        return chars;
    }

    public int myAtoi(String s) {
        if (s.length() == 0) {
            return 0;
        }
        int res = 0;
        int flag = 1; // 默认为正数
        int i = 0;
        while (i < s.length() && s.charAt(i) == ' ') {
            i++;
        }
        if (i == s.length()) {
            return 0;
        }
        if (s.charAt(i) == '-') {
            flag = -1;
        }
        if (s.charAt(i) == '+' || s.charAt(i) == '-') {
            i++;
        }
        while (i < s.length() && Character.isDigit(s.charAt(i))) {
            int r = s.charAt(i) - '0';
            if (res > Integer.MAX_VALUE / 10 || res == Integer.MAX_VALUE / 10 && r > 7) {
                return flag > 0 ? Integer.MAX_VALUE : Integer.MIN_VALUE;
            }
            res = res * 10 + r;
            i++;
        }
        return flag > 0 ? res : -res;
    }

}
