package com.kou.leetcodedemo.easy;

import com.kou.leetcodedemo.ListNode;
import com.kou.leetcodedemo.TreeNode;

import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();
        int[] arr = solution.twoSum(new int[]{2, 7, 11, 15}, 17);
        System.out.println(Arrays.toString(arr));

//        boolean is = solution.isPalindrome(121);
//        System.out.println(is);

//        int res = solution.maxSubArray(new int[]{-2, 10, -1, 2, 3, 4, 5});
//        System.out.println(res);

//        ListNode c = new ListNode(4);
//        ListNode b = new ListNode(2, c);
//        ListNode a = new ListNode(1, b);
//        ListNode f = new ListNode(4);
//        ListNode e = new ListNode(3, f);
//        ListNode d = new ListNode(1, e);
//        ListNode node = solution.mergeTwoLists(a, d);
//        System.out.println(node);
//
//        int res = solution.maxProfit(new int[]{7, 1, 5, 3, 6, 4});
//        System.out.println(res);

//        boolean valid = solution.isValid("){");
//        boolean valid = solution.isValid("()[]{}");
//        System.out.println(valid);

//        ListNode n1 = new ListNode(3);
//        ListNode n2 = new ListNode(2);
//        ListNode n3 = new ListNode(0);
//        ListNode n4 = new ListNode(-4);
//        n1.next = n2;
//        n2.next = n3;
//        n3.next = n4;
//        n4.next = n2;
//        boolean hasCycle = solution.hasCycle(n1);
//        System.out.println(hasCycle);
//        int[] num1 = new int[]{1, 2, 3, 0, 0, 0};
//        int m = 3;
//        int[] num2 = new int[]{2, 5, 6};
//        int n = 3;
//        solution.merge(num1, m, num2, n);
//        System.out.println(Arrays.toString(num1));
//        ListNode a = new ListNode(4);
//        ListNode b = new ListNode(1);
//
//        ListNode c = new ListNode(5);
//        ListNode d = new ListNode(6);
//        ListNode e = new ListNode(1);
//
//        ListNode f = new ListNode(8);
//        ListNode g = new ListNode(4);
//        ListNode h = new ListNode(5);
//        f.next = g;
//        g.next = h;
//
//        a.next = b;
//        b.next = f;
//
//        c.next = d;
//        d.next = e;
//        e.next = f;
//        ListNode node = solution.getIntersectionNode(a, c);
//        System.out.println(node);

//        String res = solution.addStrings("127", "12398");
//        System.out.println(res);
//
        TreeNode a = new TreeNode(5);
        TreeNode b = new TreeNode(4);
        TreeNode c = new TreeNode(8);

        TreeNode d = new TreeNode(11);
        TreeNode e = new TreeNode(13);
        TreeNode f = new TreeNode(4);

        TreeNode g = new TreeNode(7);
        TreeNode h = new TreeNode(2);
        TreeNode i = new TreeNode(1);
        a.left = b;
        a.right = c;
        b.left = d;
        c.left = e;
        c.right = f;
        d.left = g;
        d.right = h;
        f.right = i;
        List<Integer> resultList = solution.preorderTraversal(a);
        System.out.println(resultList);
//
        resultList = solution.inorderTraversal(a);
        System.out.println(resultList);
//
//        resultList = solution.postorderTraversal(a);
//        System.out.println(resultList);

        a = new TreeNode(2);
        b = new TreeNode(3);
        c = new TreeNode(3);

        d = new TreeNode(4);
        e = new TreeNode(5);
        f = new TreeNode(5);
        g = new TreeNode(4);

        h = new TreeNode(8);
        i = new TreeNode(9);
        TreeNode j = new TreeNode(9);
        TreeNode k = new TreeNode(8);
        a.left = b;
        a.right = c;

        b.left = d;
        b.right = e;
        c.left = f;
        c.right = g;

        e.left = h;
        e.right = i;
        g.left = j;
        g.right = k;
        boolean bo = solution.isSymmetric(a);
        System.out.println(bo);

        int maxDepth = solution.maxDepth(a);
        System.out.println(maxDepth);

//        boolean bo = solution.hasPathSum(a, 22);
//        System.out.println(bo);

//        int res = solution.search(new int[]{-1, 0, 3, 5, 9, 12}, 9);
//        int res = solution.search(new int[]{-1, 0, 3, 5, 9, 12}, 2);
//        int res = solution.search(new int[]{5}, 5);
//        System.out.println(res);

//        MyQueue queue = new MyQueue();
//        queue.push(1);
//        queue.push(2);
//        queue.push(3);
//        queue.push(4);
//        System.out.println(queue.pop());
//        queue.push(5);
//        System.out.println(queue.pop());
//        System.out.println(queue.pop());
//        System.out.println(queue.pop());
//        System.out.println(queue.peek());
//
//        MyStack stack = new MyStack();
//        stack.push(1);
//        stack.push(2);
//        System.out.println(stack.pop());
//        System.out.println(stack.top());

        int res = solution.climbStairs(3);
        System.out.println(res);

        int[] nums = new int[]{0, 0, 1, 1, 1, 2, 2, 3, 3, 4};
        int result = solution.removeDuplicates(nums);
        System.out.println(result);

        nums = new int[]{3, 2, 2, 3};
        int val = 3;
        result = solution.removeElement(nums, val);
        System.out.println(result);

        res = solution.longestPalindrome("abccccdd");
        System.out.println(res);

        nums = new int[]{-10, -3, 0, 5, 9};
        TreeNode node = solution.sortedArrayToBST(nums);
        System.out.println(node);

        a = new TreeNode(3);
        b = new TreeNode(9);
        c = new TreeNode(20);
        d = new TreeNode(15);
        e = new TreeNode(7);
        a.left = b;
        a.right = c;
        c.left = d;
        c.right = e;

        a = new TreeNode(1);
        b = new TreeNode(2);
        c = new TreeNode(3);
        d = new TreeNode(4);
        e = new TreeNode(5);
        f = new TreeNode(6);
        g = new TreeNode(7);
        a.left = b;
        a.right = c;
        b.left = d;
        c.right = e;
        d.left = f;
        e.right = g;
        boolean isBalanced = solution.isBalanced(a);
        System.out.println(isBalanced);
    }

    // 1、两数之和
    public int[] twoSum(int[] nums, int target) {
        Map<Integer, Integer> map = new HashMap<>(); // key是期望值，value是自身索引
        for (int i = 0; i < nums.length; i++) {
            if (map.containsKey(nums[i])) {
                return new int[]{map.get(nums[i]), i};
            } else {
                map.put(target - nums[i], i);
            }
        }
        return null;
    }

    public boolean isPalindrome(int x) {
        String str = Integer.toString(x);
        char[] chars = str.toCharArray();
        for (int i = 0; i < chars.length / 2; i++) {
            if (chars[i] != chars[chars.length - 1 - i]) {
                return false;
            }
        }
        return true;
    }

    public int maxSubArray(int[] nums) {
        int[] dp = new int[nums.length]; // dp[i]表示的是以num[i]结尾的全部子序列中的最大的子序列和
        dp[0] = nums[0];
        int result = dp[0];
        for (int i = 1; i < nums.length; i++) {
            dp[i] = dp[i - 1] > 0 ? nums[i] + dp[i - 1] : nums[i];
            result = Math.max(result, dp[i]);
        }
        return result;
    }

    public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        if (l1 == null) {
            return l2;
        } else if (l2 == null) {
            return l1;
        } else if (l1.val < l2.val) {
            l1.next = mergeTwoLists(l1.next, l2);
            return l1;
        } else {
            l2.next = mergeTwoLists(l2.next, l1);
            return l2;
        }
    }

    public int maxProfit(int[] prices) {
        if (prices == null || prices.length <= 1) {
            return 0;
        }
        int minPrice = Integer.MAX_VALUE;
        int maxBenefit = Integer.MIN_VALUE;
        for (int i = 1; i < prices.length; i++) { // i是第几天卖，头一天不能卖，所以i从1开始
            // 今天之前的最低价
            minPrice = Math.min(minPrice, prices[i - 1]);
            // 假如以今天之前的最低价买了，到今天能赚多少
            maxBenefit = Math.max(maxBenefit, prices[i] - minPrice);
        }
        return Math.max(maxBenefit, 0);
    }

    public boolean isValid(String s) {
        if (s == null || s.length() <= 1 || s.length() % 2 == 1) {
            return false;
        }
        Deque<Character> stack = new ArrayDeque<>();
        Map<Character, Character> map = new HashMap<>();
        map.put(']', '[');
        map.put('}', '{');
        map.put(')', '(');

        char[] chars = s.toCharArray();
        for (Character c : chars) {
            if (map.containsKey(c)) {
                if (stack.isEmpty() || !map.get(c).equals(stack.pop())) {
                    return false;
                }
            } else {
                stack.push(c);
            }
        }
        return stack.isEmpty();
    }

    public boolean hasCycle(ListNode head) {
        Set<ListNode> set = new HashSet<>();
        while (head != null) {
            set.add(head);
            head = head.next;
            if (head != null && set.contains(head)) {
                return true;
            }
        }
        return false;
    }

    public void merge(int[] nums1, int m, int[] nums2, int n) {
        int[] sorted = new int[m + n];
        int p1 = 0;
        int p2 = 0;
        while (p1 < m || p2 < n) {
            if (p1 >= m) { // 说明nums1有效值取完了,直接拼接num2的元素就行
                sorted[p1 + p2] = nums2[p2++];
            } else if (p2 >= n) { // 说明nums2有效值取完了,直接拼接nums1的元素就行
                sorted[p1 + p2] = nums1[p1++];
            } else if (nums1[p1] <= nums2[p2]) {
                sorted[p1 + p2] = nums1[p1++]; // p1++用的好，省了一行代码
            } else {
                sorted[p1 + p2] = nums2[p2++];
            }
        }
        for (int i = 0; i < nums1.length; i++) {
            nums1[i] = sorted[i];
        }
    }

    public ListNode getIntersectionNode(ListNode headA, ListNode headB) {
        if (headA == null || headB == null) {
            return null;
        }
        ListNode pA = headA;
        ListNode pB = headB;
        while (pA != pB) {
            pA = pA.next != null ? pA.next : headB;
            pB = pB.next != null ? pB.next : headA;
        }
        return pA;
    }

    public String addStrings(String s1, String s2) {
        if (s1.length() < s2.length()) {
            String temp = s1;
            s1 = s2;
            s2 = temp;
        }
        StringBuilder sb = new StringBuilder();
        int cha = s1.length() - s2.length();
        for (int i = 0; i < cha; i++) {
            sb.append(0);
        }
        s2 = sb.append(s2).toString();

        sb = new StringBuilder();
        int shang = 0;
        for (int i = s1.length() - 1; i >= 0; i--) {
            int is = s1.charAt(i) - '0';
            int js = s2.charAt(i) - '0';
            int he = is + js + shang;
            shang = he / 10;
            sb.append(he % 10);
            if (i == 0 && shang > 0) {
                sb.append(shang);
            }
        }
        return sb.reverse().toString();
    }

    // 前序遍历
    public List<Integer> preorderTraversal(TreeNode root) {
        List<Integer> list = new ArrayList<>();
        if (root == null) {
            return list;
        }
        preOrderWithList(root, list);
        return list;
    }

    // DFS的递归写法
    private void preOrderWithList(TreeNode root, List<Integer> list) {
        if (root == null) {
            return;
        }

        list.add(root.val);
        preOrderWithList(root.left, list);
        preOrderWithList(root.right, list);
    }

    // DFS的非递归写法
//    private void preOrderWithList(TreeNode root, List<Integer> list) {
//        if (root == null) {
//            return;
//        }

//        Deque<TreeNode> stack = new ArrayDeque<>();
//        stack.push(root);
//        while (!stack.isEmpty()) {
//            TreeNode node = stack.pop();
//            list.add(node.val);
//            if (node.right != null) {
//                stack.push(node.right);
//            }
//            if (node.left != null) {
//                stack.push(node.left);
//            }
//        }
//    }

    // 94、二叉树的中序遍历
    public List<Integer> inorderTraversal(TreeNode root) {
        List<Integer> list = new ArrayList<>();
        if (root != null) {
            inorderTraversalWithList(root, list);
        }
        return list;
    }

    private void inorderTraversalWithList(TreeNode node, List<Integer> list) {
        if (node.left != null) {
            inorderTraversalWithList(node.left, list);
        }
        list.add(node.val);
        if (node.right != null) {
            inorderTraversalWithList(node.right, list);
        }
    }

    // 后序遍历
    public List<Integer> postorderTraversal(TreeNode root) {
        List<Integer> list = new ArrayList<>();
        if (root == null) {
            return list;
        }
        postorderTraversalWithList(root, list);
        return list;
    }

    private void postorderTraversalWithList(TreeNode root, List<Integer> list) {
        if (root == null) {
            return;
        }
        if (root.left != null) {
            postorderTraversalWithList(root.left, list);
        }
        if (root.right != null) {
            postorderTraversalWithList(root.right, list);
        }
        list.add(root.val);
    }

    public boolean hasPathSum(TreeNode root, int sum) {
        if (root == null) {
            return false;
        }
        // System.out.println(root.val); // 查看二叉树各结点访问顺序
        if (root.left == null && root.right == null) {
            return root.val == sum;
        }
        return hasPathSum(root.right, sum - root.val) || hasPathSum(root.left, sum - root.val);
    }

    public ListNode middleNode(ListNode head) {
        if (head == null) {
            return null;
        }
        List<ListNode> list = new ArrayList<>();
        while (head != null) {
            list.add(head);
            head = head.next;
        }
        return list.get(list.size() / 2);
    }

    public int search(int[] nums, int target) {
        int i = 0;
        int j = nums.length - 1;
        while (i <= j) {
            int mid = i + (j - i) / 2; // 之所以不用mid=(i+j)/2，是防止整型溢出。会溢出吗？
            if (nums[mid] == target) {
                return mid;
            }
            if (nums[mid] < target) {
                i = mid + 1; // 从[mid+1,j]区间查(因为num[mid]!=target，所以没必要再查mid处了)
            } else {
                j = mid - 1; // 同理，从[i,mid-1]区间查
            }
        }
        return -1;
    }

    public int climbStairs(int n) {
        if (n < 0) {
            return 0;
        } else if (n <= 2) {
            return n;
        }

        int[] dp = new int[n + 1];
        dp[1] = 1;
        dp[2] = 2;
        for (int i = 3; i <= n; i++) {
            dp[i] = dp[i - 1] + dp[i - 2];
        }
        return dp[n];
    }

    // 相同的树
    public boolean isSameTree(TreeNode p, TreeNode q) {
        if (p == null && q == null) {
            return true;
        } else if (p == null || q == null) {
            return false;
        }
        return p.val == q.val && isSameTree(p.left, q.left) && isSameTree(p.right, q.right);
    }

    // 101、对称二叉树
    // 给你一个二叉树的根结点 root ， 检查它是否轴对称。
    public boolean isSymmetric(TreeNode root) {
        return compareNode(root.left, root.right);
    }

    // 判断两个子树的的值是否相等
    private boolean compareNode(TreeNode a, TreeNode b) {
        if (a == null && b == null) {
            return true;
        } else if (a == null) {
            return false;
        } else {
            return a.val == b.val && compareNode(a.left, b.right) && compareNode(a.right, b.left);
        }
    }

    // 104、二叉树的最大深度
    public int maxDepth(TreeNode root) {
        if (root == null) {
            return 0;
        } else {
            return Math.max(maxDepth(root.left), maxDepth(root.right)) + 1;
        }
    }

    //
    public int removeDuplicates(int[] nums) {
        int p = 0; // 慢指针
        int q = 1;// 快指针
        while (q < nums.length) {
            if (nums[q] != nums[p]) {
                nums[p + 1] = nums[q];
                p++;
            }
            q++;
        }
        return p + 1;
    }

    // 27、移除元素
    public int removeElement(int[] nums, int val) {
        int slow = 0;
        for (int i = 0; i < nums.length; i++) {
            int element = nums[i];
            if (element != val) {
                nums[slow] = element;
                slow++;
            }
        }
        return slow;
    }

    // 409、最长回文串
    public int longestPalindrome(String s) {
        int res = 0;
        char[] chars = s.toCharArray();
        Map<Character, Integer> map = new HashMap<>();
        for (char c : chars) {
            int cc = map.getOrDefault(c, 0);
            if (cc == 0) {
                map.put(c, 1);
            } else {
                map.put(c, cc + 1);
            }
        }

        Set<Map.Entry<Character, Integer>> set = map.entrySet();
        boolean b = false;
        for (Map.Entry<Character, Integer> se : set) {
            int cc = se.getValue();
            if (cc % 2 == 0) {
                res = res + cc;
            } else {
                res = res + cc / 2 * 2;
                b = true;
            }
        }
        if (b) {
            res = res + 1;
        }
        return res;
    }

    // 108、将有序数组转换为二叉搜索树
    public TreeNode sortedArrayToBST(int[] nums) {
        return sortedArrayToBST(nums, 0, nums.length - 1);
    }

    // 包括start和end
    private TreeNode sortedArrayToBST(int[] nums, int start, int end) {
        if (start > end) {
            return null;
        }

        if (start == end) {
            return new TreeNode(nums[start]);
        } else if (end - start == 1) {
            return new TreeNode(nums[end], new TreeNode(nums[start]), null);
        } else {
            // 选择中间的值作为根节点值
            int r = (start + end) / 2;
            return new TreeNode(nums[r], sortedArrayToBST(nums, start, r - 1), sortedArrayToBST(nums, r + 1, end));
        }
    }

    // 110、平衡二叉树 可以用作一面的面试题，不算简单，也不算难
    // 平衡二叉树的定义为每个结点的左、右子树高度差不大于1
    // 求root的高度，如果值不是-1，则表示该树是平衡二叉树，否则不是
    public boolean isBalanced(TreeNode root) {
        return height(root) != -1;
    }

    // 求root的高度
    public int height(TreeNode root) {
        if (root == null) {
            System.out.println("高度为0");
            return 0;
        }

        int leftHeight = height(root.left);
        if (leftHeight == -1) {
            System.out.println(root.val + "-0高度为-1");
            return -1;
        }
        int rightHeight = height(root.right);
        if (rightHeight == -1) {
            System.out.println(root.val + "-1高度为-1");
            return -1;
        }

        if (Math.abs(leftHeight - rightHeight) > 1) {
            System.out.println(root.val + "-2高度为-1");
            return -1;
        }
        System.out.println(root.val + "-3高度为" + (Math.max(leftHeight, rightHeight) + 1));
        return Math.max(leftHeight, rightHeight) + 1;
    }
}