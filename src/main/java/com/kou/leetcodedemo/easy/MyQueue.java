package com.kou.leetcodedemo.easy;

import java.util.ArrayDeque;
import java.util.Deque;

// 用栈实现队列的功能
// 只能使用标准的栈操作，也就是只有 push to top, peek/pop from top, size, 和 is empty 操作是合法的。
class MyQueue {
    Deque<Integer> inStack; // 存入数据时放这个栈
    Deque<Integer> outStack;// 取数据时从这个栈拿

    public MyQueue() {
        inStack = new ArrayDeque<>();
        outStack = new ArrayDeque<>();
    }

    public void push(int x) {
        inStack.push(x);
    }

    public int pop() {
        if (outStack.isEmpty()) { // 除非outStack为空，否则不从inStack转移数据过来
            in2out();
        }
        return outStack.pop();
    }

    public int peek() {
        if (outStack.isEmpty()) {
            in2out();
        }
        return outStack.peek();
    }

    public boolean empty() {
        return inStack.isEmpty() && outStack.isEmpty();
    }

    private void in2out() {
        while (!inStack.isEmpty()) {
            outStack.push(inStack.pop());
        }
    }
}


/**
 * Your MyQueue object will be instantiated and called as such:
 * MyQueue obj = new MyQueue();
 * obj.push(x);
 * int param_2 = obj.pop();
 * int param_3 = obj.peek();
 * boolean param_4 = obj.empty();
 */