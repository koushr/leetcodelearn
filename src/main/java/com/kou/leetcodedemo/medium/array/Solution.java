package com.kou.leetcodedemo.medium.array;

import com.kou.leetcodedemo.TreeNode;

import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();
        int[] nums = new int[]{-1, 0, 1, 2, -1, -4};
        List<List<Integer>> result = solution.threeSum(nums);
        System.out.println(result);

        nums = new int[]{-1, 2, 1, -4};
        int target = 1;
        int threeSumClosest = solution.threeSumClosest(nums, target);
        System.out.println(threeSumClosest);

        nums = new int[]{2, 2, 2, 2, 2, 2};
        target = 8;
        nums = new int[]{-2, -1, -1, 1, 1, 2, 2};
        target = 0;
        nums = new int[]{1000000000, 1000000000, 1000000000, 1000000000, 1000000000};// 每个都是10亿
        target = -294967296;
        nums = new int[]{-1000000000, -1000000000, -1000000000, -1000000000, -1000000000};
        target = 294967296;
        result = solution.fourSum(nums, target);
        System.out.println(result);

        nums = solution.sortArray(new int[]{2, 8, 4, 1, 3, 5, 6, 7});
        System.out.println("快排后" + Arrays.toString(nums));
//
        int res = solution.findKthLargest(new int[]{3, 2, 1, 5, 6, 4}, 2);
        System.out.println(res);
//
        TreeNode t15 = new TreeNode(15);
        TreeNode t7 = new TreeNode(7);
        TreeNode t20 = new TreeNode(20, t15, t7);
        TreeNode t9 = new TreeNode(9);
        TreeNode root = new TreeNode(3, t9, t20);

//        TreeNode node = solution.lowestCommonAncestor(root, t15, t9);
//        System.out.println(node);

//        int res = solution.search(new int[]{6, 7, 8, 9, 0, 1, 2, 3, 4, 5}, 8);
//        System.out.println(res);

//        char[][] chars = new char[4][];
//        chars[0] = new char[]{'1', '1', '0', '0', '0'};
//        chars[1] = new char[]{'1', '1', '0', '0', '0'};
//        chars[2] = new char[]{'0', '0', '1', '0', '0'};
//        chars[3] = new char[]{'0', '0', '0', '1', '1'};
//        int result = solution.numIslands(chars);
//        System.out.println(result);

//        List<List<Integer>> resultList = solution.permute(new int[]{1, 2, 3});
//        System.out.println(resultList);

        res = solution.lengthOfLIS(new int[]{2, 3, 6, 5, 9, 10, 4, 8, 11, 7});
        System.out.println(res);

        String maxDictionaryOrder = solution.maxDictionaryOrder("aabcbccacbbcbaaba");
        System.out.println(maxDictionaryOrder);

        res = solution.singleNumber(new int[]{0, 1, 0, 1, 0, 1, 100});
        System.out.println(res);

        int maxArea = solution.maxArea(new int[]{1, 8, 6, 2, 5, 4, 8, 3, 7});
        System.out.println(maxArea);
    }

    // 给你一个整数数组 nums ，判断是否存在三元组 [nums[i], nums[j], nums[k]] 满足 i != j、i != k 且 j != k ，同时还满足 nums[i] + nums[j] + nums[k] == 0 。请
    // 你返回所有和为 0 且不重复的三元组。
    // 注意：答案中不可以包含重复的三元组。
    // 15、三数之和
    public List<List<Integer>> threeSum(int[] nums) {
        List<List<Integer>> res = new ArrayList<>();
        if (nums == null || nums.length < 3) {
            return res;
        }
        Arrays.sort(nums);
        int length = nums.length;
        for (int i = 0; i < length - 2; i++) { // i是第一个,l是第二个,r是第三个
            if (nums[i] > 0) {
                continue;
            }

            if (i > 0 && nums[i] == nums[i - 1]) {
                continue;
            }
            int l = i + 1;
            int r = length - 1;
            while (l < r) {
                int sum = nums[i] + nums[l] + nums[r];
                if (sum < 0) {
                    l++;
                } else if (sum > 0) {
                    r--;
                } else {
                    res.add(List.of(nums[i], nums[l], nums[r]));
                    while (l < r && nums[l + 1] == nums[l]) { // TODO while条件到底是什么
                        l++;
                    }
                    while (l < r && nums[r - 1] == nums[r]) {
                        r--;
                    }
                    l++;
                    r--;
                }
            }
        }
        return res;
    }

    // 给你一个长度为 n 的整数数组 nums 和 一个目标值 target。请你从 nums 中选出三个整数，使它们的和与 target 最接近。
    // 返回这三个数的和。
    // 假定每组输入只存在恰好一个解。
    // 16、最接近的三数之和
    public int threeSumClosest(int[] nums, int target) {
        Arrays.sort(nums);
        int length = nums.length;
        int minDiff = Integer.MAX_VALUE;
        int closetSum = nums[0] + nums[1] + nums[2];
        for (int i = 0; i < length - 2; i++) {
            int left = i + 1;
            int right = length - 1;
            while (left < right) {
                int sum = nums[i] + nums[left] + nums[right];
                int diff = Math.abs(sum - target);
                if (diff == 0) {
                    return target;
                }
                if (diff < minDiff) {
                    minDiff = diff;
                    closetSum = sum;
                }

                if (sum - target < 0) {
                    left++;
                } else {
                    right--;
                }
            }
        }
        return closetSum;
    }

    // 给你一个由 n 个整数组成的数组 nums ，和一个目标值 target 。请你找出并返回满足下述全部条件且不重复的四元组 [nums[a], nums[b], nums[c], nums[d]] （若两个四元组元素一一对应，则认为两个四元组重复）：
    // 0 <= a, b, c, d < n
    // a、b、c 和 d 互不相同
    // nums[a] + nums[b] + nums[c] + nums[d] == target
    // 你可以按 任意顺序 返回答案 。
    // 18、四数之和
    public List<List<Integer>> fourSum(int[] nums, int target) {
        List<List<Integer>> resultList = new ArrayList<>();
        int length = nums.length;
        if (length < 4) {
            return resultList;
        }
        if (length == 4) {
            if ((long) nums[0] + nums[1] + nums[2] + nums[3] == target) { // long+int，返回值是long
                resultList.add(List.of(nums[0], nums[1], nums[2], nums[3]));
            }
            return resultList;
        }

        Arrays.sort(nums);
        for (int i = 0; i < length - 3; i++) {
            if (i > 0 && nums[i] == nums[i - 1]) {
                continue;
            }

            // 求和为target的三个数
            for (int j = i + 1; j < length - 2; j++) {
                if (j > i + 1 && nums[j] == nums[j - 1]) {
                    continue;
                }

                int curTarget = target - nums[i];
                int left = j + 1;
                int right = length - 1;
                while (left < right) {
                    if ((long) nums[left] + nums[right] + nums[j] < curTarget) { // 转成long型处理越界
                        left++;
                    } else if ((long) nums[left] + nums[right] + nums[j] > curTarget) {
                        right--;
                    } else {
                        resultList.add(List.of(nums[i], nums[j], nums[left], nums[right]));
                        while (nums[left + 1] == nums[left] && left + 1 < right) {
                            left++;
                        }

                        while (nums[right - 1] == nums[right] && right - 1 > left) {
                            right--;
                        }

                        left++;
                        right--;
                    }
                }
            }
        }
        return resultList;
    }

    // 返回数组中第 k 个最大的元素
    public int findKthLargest(int[] nums, int k) {
        return singlePivotQuickSort(nums, 0, nums.length - 1, nums.length - k); // 第 k 个最大的元素的所有是nums.length-k
    }

    // 返回数组中第 k 个最大的元素
    private int singlePivotQuickSort(int[] nums, int left, int right, int k) {
        if (right > left) {
            int pivotIndex = partition(nums, left, right);
            if (pivotIndex == k) {
                return nums[k];
            }
            if (pivotIndex < k) {
                singlePivotQuickSort(nums, pivotIndex + 1, right, k);
            } else {
                singlePivotQuickSort(nums, left, pivotIndex - 1, k);
            }
        }
        return nums[k];
    }

    private final Random random = new Random();

    // 力扣912、排序数组
    public int[] sortArray(int[] nums) {
        if (nums == null || nums.length <= 1) {
            return nums;
        }
        singlePivotQuickSort(nums, 0, nums.length - 1);
        return nums;
    }

    private void singlePivotQuickSort(int[] nums, int left, int right) {
        if (left >= right) {
            return;
        }
        int pivotIndex = partition(nums, left, right);
        singlePivotQuickSort(nums, left, pivotIndex - 1);
        singlePivotQuickSort(nums, pivotIndex + 1, right);
    }

    // 生成一个轴，轴左边的元素都小于轴，轴右边的元素都大于轴
    private int partition(int[] nums, int left, int right) {
        // 第一步：随机生成一个位置作为主元元素位置，把主元元素和最后一个元素互换位置
        int initPivotIndex = random.nextInt(left, right + 1);
        swap(nums, initPivotIndex, right);

        // TODO i指针从left-1开始，j指针从left开始。i指针的起始位置一直记不住
        // 当j对应的元素值小于主元元素值时，往右移动i，并将i对应的元素和j对应的元素互换位置，之后右移j，再执行比较和互换逻辑，一直到j对应最后一个元素
        // 最后再把i+1对应的元素和主元元素互换
        // 返回i+1
        int i = left - 1;
        for (int j = left; j < right; j++) {
            if (nums[j] < nums[right]) {
                i++; // TODO 在swap之前先给i加1 总是忘
                swap(nums, i, j);
            }
        }
        swap(nums, i + 1, right);
        return i + 1;
    }

    private void swap(int[] nums, int i, int j) {
        if (i != j) {
            System.out.println("索引" + i + "处的" + nums[i] + "和索引" + j + "处的" + nums[j] + "交换位置");
        }
        if (nums.length <= 1 || i == j) {
            return;
        }
        int tmp = nums[i];
        nums[i] = nums[j];
        nums[j] = tmp;
    }

    // 给定一个二叉树, 找到该树中两个指定结点的最近公共祖先
    public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
        if (root == null || root == p || root == q) {
            return root;
        }
        TreeNode left = lowestCommonAncestor(root.left, p, q);
        TreeNode right = lowestCommonAncestor(root.right, p, q);
        if (left != null && right != null) {
            return root;
        }
        return left == null ? right : left;
    }

    public int search(int[] nums, int target) {
        if (nums == null || nums.length == 0) {
            return -1;
        }
        int n = nums.length;
        if (n == 1) {
            return nums[0] == target ? 0 : -1;
        }
        int l = 0;
        int r = n - 1;
        while (l <= r) {
            if (nums[l] == target) {
                return l;
            }
            if (nums[r] == target) {
                return r;
            }
            int mid = (l + r) / 2;
            if (nums[mid] == target) {
                return mid;
            }
            if (nums[l] == nums[mid]) {
                return -1;
            }
            if (nums[l] < nums[mid]) { // 左半边升序的情况下，优先去左半边找，如果在左半边，则就在左半边找出来，否则去右半边找
                if (nums[l] < target && target < nums[mid]) {
                    r = mid - 1;
                } else {
                    l = mid + 1;
                }
            } else { // 右半边升序的情况下，优先去右半边找，如果在右半边，则就在右半边找出来，否则去左半边找
                if (nums[mid] < target && target < nums[r]) {
                    l = mid + 1;
                } else {
                    r = mid - 1;
                }
            }
        }
        return -1;
    }

//    给你一个由 '1'（陆地）和 '0'（水）组成的的二维网格，请你计算网格中岛屿的数量。
//    岛屿总是被水包围，并且每座岛屿只能由水平方向和/或竖直方向上相邻的陆地连接形成。
//    此外，你可以假设该网格的四条边均被水包围。

    //    输入：grid = [
//            ['1','1','1','1','0'],
//            ['1','1','0','1','0'],
//            ['1','1','0','0','0'],
//            ['0','0','0','0','0']
//            ]
//    输出：1

    public int numIslands(char[][] grid) {
        int result = 0;
        for (int r = 0; r < grid.length; r++) {
            for (int c = 0; c < grid[r].length; c++) {
                if (grid[r][c] == '1') {
                    result++;
                    infectAroundToZero(grid, r, c);
                }
            }
        }
        return result;
    }

    // 把当前结点及能触达到的值不为0的结点的值都修改成0
    private void infectAroundToZero(char[][] grid, int r, int c) {
        grid[r][c] = '0';
        if (r - 1 >= 0 && c < grid[r].length && grid[r - 1][c] != '0') {
            infectAroundToZero(grid, r - 1, c); // 上
        }
        if (r + 1 < grid.length && c < grid[r].length && grid[r + 1][c] != '0') {
            infectAroundToZero(grid, r + 1, c); // 下
        }

        if (c - 1 >= 0 && c - 1 < grid[r].length && grid[r][c - 1] != '0') {
            infectAroundToZero(grid, r, c - 1); // 左
        }
        if (c + 1 < grid[r].length && grid[r][c + 1] != '0') {
            infectAroundToZero(grid, r, c + 1); // 右
        }
    }

    // 全排列。用回溯法
    public List<List<Integer>> permute(int[] nums) {
        List<List<Integer>> resultList = new ArrayList<>();
        if (nums == null || nums.length == 0) {
            return resultList;
        } else if (nums.length == 1) {
            resultList.add(List.of(nums[0]));
            return resultList;
        }
        dfs(nums, 0, new boolean[nums.length], new ArrayDeque<>(), resultList);
        return resultList;
    }

    private void dfs(int[] nums, int depth, boolean[] used, Deque<Integer> stack, List<List<Integer>> resultList) {
        if (depth == nums.length) {
            resultList.add(new ArrayList<>(stack));
            return;
        }
        for (int i = 0; i < nums.length; i++) {
            if (!used[i]) {
                stack.offerLast(nums[i]);
                used[i] = true;
                dfs(nums, depth + 1, used, stack, resultList);
                used[i] = false; // 回退1
                stack.pollLast(); // 回退2
            }
        }
    }

    // 给你一个整数数组 nums ，找到其中最长严格递增子序列的长度。
    // 子序列 是由数组派生而来的序列，删除（或不删除）数组中的元素而不改变其余元素的顺序。例如，[3,6,2,7] 是数组 [0,3,1,6,2,2,7] 的子序列。
    // 示例 1：
    // 输入：nums = [10,9,2,5,3,7,101,18]
    // 输出：4
    // 解释：最长递增子序列是 [2,3,7,101]，因此长度为 4 。
    // 示例 2：
    // 输入：nums = [0,1,0,3,2,3]
    // 输出：4
    public int lengthOfLIS(int[] nums) {
        // tails[k]的值代表长度为k+1的递增子序列中最小的递增子序列的尾部元素值
        // 长度为k+1的递增子序列可能有很多，我们这里找的是最小的递增子序列
        int[] tails = new int[nums.length];
        int res = 0; // res代表tails有多少个有效元素
        for (int num : nums) {
            if (res == 0 || num > tails[res - 1]) {
                tails[res++] = num;
            } else {
                int i = 0, j = res; // i和j分别是tails有效值的左边界索引和右边界索引，包括i，不包括j
                // 如果num大于tails最大的有效值，则把num插入到原最大值之后，称为最新的最大值。否则，利用二分查找把大于它的元素中最小的那个覆盖掉
                while (i < j) { // while循环就是二分查找，找最小的大于num的元素。退出循环的条件就是i等于j，i不可能大于j
                    int m = (i + j) / 2;
                    if (tails[m] < num) {
                        i = m + 1;
                    } else {
                        j = m;
                    }
                }
                tails[i] = num;
            }
        }
        return res; // 为什么res的值或者说tails的有效元素数量就是想要的结果
    }

    // 牛客网 字典序的最大子序列
    public String maxDictionaryOrder(String s) {
        char[] chs = s.toCharArray();
        StringBuilder sb = new StringBuilder();
        int maxIdx = -1;
        do {
            char maxCur = Character.MIN_VALUE;
            for (int i = maxIdx + 1; i < chs.length; i++) {
                if (chs[i] > maxCur) {
                    maxCur = chs[i];
                    maxIdx = i;
                }
            }
            sb.append(maxCur);
        } while (maxIdx != chs.length - 1);
        return sb.toString();
    }

    public int singleNumber(int[] nums) {
        Map<Integer, Integer> map = new HashMap<>(); // key是元素值，value是次数
        for (Integer e : nums) {
            Integer time = map.get(e);
            if (time == null) {
                map.put(e, 1);
            } else {
                if (time == 2) {
                    map.remove(e);
                } else {
                    map.put(e, time + 1);
                }
            }
        }
        return (Integer) (map.keySet().toArray()[0]);
    }

    // 11、盛最多水的容器
    // 给定一个长度为 n 的整数数组 height 。有 n 条垂线，第 i 条线的两个端点是 (i, 0) 和 (i, height[i]) 。
    // 找出其中的两条线，使得它们与 x 轴共同构成的容器可以容纳最多的水。
    // 返回容器可以储存的最大水量。
    // 说明：你不能倾斜容器。
    public int maxArea(int[] height) {
        int maxArea = 0;
        int left = 0; // 左指针索引
        int right = height.length - 1;// 右指针索引

        while (left < right) {
            maxArea = Math.max(Math.min(height[left], height[right]) * (right - left), maxArea);
            if (height[left] <= height[right]) {
                left++;
            } else {
                right--;
            }
        }
        return maxArea;
    }
}
