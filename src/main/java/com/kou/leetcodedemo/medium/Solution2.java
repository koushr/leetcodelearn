package com.kou.leetcodedemo.medium;

import com.kou.leetcodedemo.ListNode;

public class Solution2 {
    public static void main(String[] args) {
        Solution2 solution2 = new Solution2();
//        String res = solution2.multiply("123456789", "12345678");
//        System.out.println(res);
//        boolean[][] dp = new boolean[3][4];
//        System.out.println(Arrays.deepToString(dp));
        int res = solution2.uniquePaths(3, 7);
        System.out.println(res);
//        System.out.println(dp.length);

        int[][] obstacleGrid = new int[3][2];
        obstacleGrid[0] = new int[]{0, 0};
        obstacleGrid[1] = new int[]{1, 1};
        obstacleGrid[2] = new int[]{0, 0};
        res = solution2.uniquePathsWithObstacles(obstacleGrid);
        System.out.println(res);

//        ListNode listNodeA3 = new ListNode(3);
        ListNode listNodeA2 = new ListNode(4);
        ListNode listNodeA1 = new ListNode(6, listNodeA2);
//        ListNode re = solution2.reverseList(listNodeA1);
//        System.out.println(re);

//        ListNode listNodeB3 = new ListNode(4);
        ListNode listNodeB2 = new ListNode(6);
        ListNode listNodeB1 = new ListNode(5, listNodeB2);
        ListNode result = solution2.addTwoNumbers(listNodeA1, listNodeB1);
        System.out.println(result);
    }

//    public String multiply(String num1, String num2) {
//        if (num1.equals("0") || num2.equals("0")) {
//            return "0";
//        }
//        String ans = "0";
//        int m = num1.length();
//        int n = num2.length();
//        for (int i = n - 1; i >= 0; i--) {
//            StringBuilder curr = new StringBuilder();
//            int add = 0;// 进位值
//            for (int j = 0; j < n - 1 - i; j++) {
//                curr.append(0);
//            }
//            int y = num2.charAt(i) - '0';
//            for (int j = m - 1; j >= 0; j--) {
//                int x = num1.charAt(j) - '0';
//                int product = x * y + add;
//                curr.append(product % 10);// 取余
//                add = product / 10;// 取整
//            }
//            // 当计算到最高位时，如果 x * y + add < 10，则add就会为0，否则add就会大于0
//            if (add != 0) {
//                curr.append(add);// 递进一位
//            }
//            String str = curr.reverse().toString();
//            ans = addStrings(ans, str);
//        }
//        System.out.println(ans);
//        return ans;
//    }

    // 62、不同路径 med
    public int uniquePaths(int m, int n) {
        if (m == n && m == 1) {
            return 1;
        }
        int[][] dp = new int[m][n];
        for (int i = 0; i < m; i++) { // 第一行的索引为0，第一列的索引为0
            for (int j = 0; j < n; j++) {
                if (i == 0 || j == 0) {
                    dp[i][j] = 1;
                } else {
                    dp[i][j] = dp[i - 1][j] + dp[i][j - 1];
                }
            }
        }
        return dp[m - 1][n - 1];
    }

    // 63、不同路径II med
    public int uniquePathsWithObstacles(int[][] obstacleGrid) {
        if (obstacleGrid[0][0] == 1) {
            return 0;
        }

        int c = obstacleGrid[0].length;
        int r = obstacleGrid.length;
        int[][] dp = new int[r][c];
        for (int i = 0; i < r; i++) { // 遍历行
            for (int j = 0; j < c; j++) { // 遍历列
                if (j == 0 && i == 0) {
                    dp[i][j] = 1;
                } else {
                    dp[i][j] = obstacleGrid[i][j] == 1 ? 0 : (i - 1 < 0 ? 0 : dp[i - 1][j]) + (j - 1 < 0 ? 0 : dp[i][j - 1]);
                }
            }
        }
        return dp[r - 1][c - 1];
    }

    public ListNode reverseList(ListNode head) {
        ListNode prev = null;
        while (head != null) { // 如果一时搞不清楚跳出循环的条件，可以先写一个很大的for循环，等最后抛异常时，看报错是什么，就知道什么时候跳出循环了
            ListNode next = head.next; // 把下一个结点先记录起来，下面要用
            head.next = prev; // 指针反转
            prev = head; // prev往尾结点走一步
            head = next; // head往尾结点走一步
        }
        return prev;
    }

    // 2、两数相加
    // 给你两个非空的链表，表示两个非负的整数。它们每位数字都是按照逆序的方式存储的，并且每个结点只能存储一位数字。
    // 请你将两个数相加，并以相同形式返回一个表示和的链表。
    // 你可以假设除了数字0之外，这两个数都不会以0开头。
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        int add = 0;
        ListNode head = new ListNode();
        ListNode hair = head;
        while (l1 != null || l2 != null) {
            int l1V = 0;
            if (l1 != null) {
                l1V = l1.val;
            }

            int l2V = 0;
            if (l2 != null) {
                l2V = l2.val;
            }
            int he = l1V + l2V + add;
            add = he / 10;
            ListNode listNode = new ListNode(he % 10);
            head.next = listNode;
            head = listNode;
            if (l1 != null) {
                l1 = l1.next;
            }
            if (l2 != null) {
                l2 = l2.next;
            }
        }
        if (add > 0) {
            head.next = new ListNode(add);
        }
        return hair.next;
    }

    // 37、两整数之和
    public int getSum(int a, int b) { // 考位运算
        return Math.addExact(a, b);
    }
}
