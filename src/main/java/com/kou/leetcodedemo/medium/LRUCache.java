package com.kou.leetcodedemo.medium;

import java.util.HashMap;

// 请你设计并实现一个满足 LRU (最近最少使用) 缓存 约束的数据结构。
// 实现 LRUCache 类：
// LRUCache(int capacity) 以 正整数 作为容量capacity 初始化 LRU 缓存
// int get(int key) 如果关键字 key 存在于缓存中，则返回关键字的值，否则返回 -1 。
// void put(int key, int value)如果关键字key 已经存在，则变更其数据值value ；如果不存在，则向缓存中插入该组key-value 。如果插入操作导致关键字数量超过capacity ，则应该 逐出 最久未使用的关键字。
// 函数 get 和 put 必须以 O(1) 的平均时间复杂度运行。
public class LRUCache {
    private static class DoubleLinkedNode {
        private int key;
        private int value;

        public DoubleLinkedNode() {
        }

        public DoubleLinkedNode(int key, int value) {
            this.key = key;
            this.value = value;
        }

        private DoubleLinkedNode prev;
        private DoubleLinkedNode next;

    }

    HashMap<Integer, DoubleLinkedNode> cache = new HashMap<>();
    int capacity;
    DoubleLinkedNode head; // 虚拟头结点
    DoubleLinkedNode tail; // 虚拟尾结点

    public LRUCache(int capacity) {
        this.capacity = capacity;
        head = new DoubleLinkedNode();
        tail = new DoubleLinkedNode();
        head.next = tail;
        tail.prev = head;
    }

    public int get(int key) {
        DoubleLinkedNode node = cache.get(key);
        if (node == null) {
            return -1;
        }
        moveToHead(node);
        return node.value;
    }

    public void moveToHead(DoubleLinkedNode node) {
        removeNode(node);
        addToHead(node);
    }

    public void removeNode(DoubleLinkedNode node) {
        DoubleLinkedNode oriPrev = node.prev;
        DoubleLinkedNode oriNext = node.next;

        oriPrev.next = oriNext;
        oriNext.prev = oriPrev;
    }

    public void addToHead(DoubleLinkedNode node) {
        node.prev = head; // 新结点的prev设置为虚拟头结点
        node.next = head.next;// 新结点的next设置为原第一个数据结点

        head.next.prev = node;// 原第一个数据结点的prev设置为新结点

        head.next = node;// 虚拟头结点的next设置为新结点
    }

    public void put(int key, int value) {
        DoubleLinkedNode node = cache.get(key);
        if (node == null) {
            node = new DoubleLinkedNode(key, value);
            addToHead(node);
            if (cache.size() >= capacity) {
                cache.remove(tail.prev.key);
                removeNode(tail.prev);
            }
            cache.put(key, node);
        } else {
            node.value = value;
            moveToHead(node);
        }
    }

//    public static void main(String[] args) {
//        int capacity = 2;
//        LRUCache obj = new LRUCache(capacity);
//        obj.put(1, 1);
//        obj.put(2, 2);
//        int value = obj.get(1);
//        System.out.println(value);
//        obj.put(3, 3);
//        value = obj.get(2);
//        System.out.println(value);
//        obj.put(4, 4);
//        value = obj.get(1);
//        System.out.println(value);
//        value = obj.get(3);
//        System.out.println(value);
//        value = obj.get(4);
//        System.out.println(value);
//        value = obj.get(2);
//        System.out.println(value);
//    }
}
