package com.kou.leetcodedemo.medium;

import com.kou.leetcodedemo.ListNode;
import com.kou.leetcodedemo.TreeNode;

import java.util.*;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();
        int l = solution.lengthOfLongestSubstring("abcabcbb");
        System.out.println(l);

//        int l2 = solution.equalSubstring("pxezla", "loewbi", 25);
        int l2 = solution.equalSubstring("abcd", "bcdf", 3);
        System.out.println(l2);

        String result = solution.longestPalindrome("xaabacxcabaaxcabaax");
        System.out.println(result);

        ListNode a = new ListNode(1);
        ListNode b = new ListNode(2);
        ListNode c = new ListNode(3);
        ListNode d = new ListNode(4);
        ListNode e = new ListNode(5);
        a.next = b;
        b.next = c;
        c.next = d;
        d.next = e;
//        ListNode res = solution.reverseBetween(a, 2, 4);
//        System.out.println(res);

        ListNode ar = solution.rotateRight(a, 3);
        System.out.println(ar);

        ListNode res = solution.removeNthFromEnd(a, 2);
        System.out.println(res);

//        int[][] matrix = new int[5][5];
//        matrix[0] = new int[]{1, 2, 3, 4, 5};
//        matrix[1] = new int[]{6, 7, 8, 9, 10};
//        matrix[2] = new int[]{11, 12, 13, 14, 15};
//        matrix[3] = new int[]{16, 17, 18, 19, 20};
//        matrix[4] = new int[]{21, 22, 23, 24, 25};
//        int[][] matrix = new int[2][];
//        matrix[0] = new int[]{3};
//        matrix[1] = new int[]{2};
//        List<Integer> resultList = solution.spiralOrder(matrix);
//        System.out.println(resultList);

//        ListNode a = new ListNode(1);
//        ListNode b = new ListNode(2);
//        ListNode c = new ListNode(3);
//        ListNode d = new ListNode(4);
//        ListNode e = new ListNode(5);
//        a.next = b;
//        b.next = c;
//        c.next = d;
//        d.next = e;
//        ListNode res = solution.detectCycle(a);
//        System.out.println(res);
//
//        solution.reorderList(a);

        TreeNode at = new TreeNode(3);
        TreeNode bt = new TreeNode(1);
        TreeNode ct = new TreeNode(4);
        TreeNode dt = new TreeNode(2);
//        TreeNode et = new TreeNode(7);
        at.left = bt;
        at.right = ct;
        ct.left = dt;
//        ct.right = et;
//        List<Integer> list = solution.rightSideView(a);
//        System.out.println(list);

        boolean is = solution.isValidBST(at);
        System.out.println(is);

        solution.recoverTree(at);

        //[[1,3],[2,6],[8,10],[15,18]]
//        [2,3],[4,5],[6,7],[8,9],[1,10]
//        int[][] res = solution.merge(new int[][]{new int[]{2, 3}, new int[]{4, 5}, new int[]{6, 7}, new int[]{8, 9}, new int[]{1, 10}});
//        int[][] res = solution.merge(new int[][]{new int[]{1, 4}, new int[]{2, 3}});
//        System.out.println(Arrays.deepToString(res));

        int[] nums = new int[]{1, 3, 2};
        solution.nextPermutation(nums);
        System.out.println(Arrays.toString(nums));

        nums = new int[]{2, 0, 2, 1, 1, 0};
        solution.sortColors(nums);
        System.out.println(Arrays.toString(nums));

        List<TreeNode> treeNodes = solution.generateTrees(5);
        System.out.println(treeNodes);

        int cc = solution.numTrees(5);
        System.out.println(cc);

        TreeNode aat = new TreeNode(1);
        TreeNode abt = new TreeNode(2);
        TreeNode act = new TreeNode(3);
        TreeNode adt = new TreeNode(4);
        TreeNode aet = new TreeNode(5);
        TreeNode aft = new TreeNode(6);
        TreeNode agt = new TreeNode(7);
        TreeNode aht = new TreeNode(8);
        aat.left = abt;
        aat.right = act;

        abt.left = adt;
        act.right = aet;

        adt.left = aft;
        adt.right = agt;
        aet.right = aht;
        List<List<Integer>> resultList = solution.levelOrderTraversal(aat);
        System.out.println(resultList);

        aat = new TreeNode(1);
        abt = new TreeNode(2);
        act = new TreeNode(3);
        adt = new TreeNode(4);
        aet = new TreeNode(5);
        aft = new TreeNode(6);
        agt = new TreeNode(7);
        aat.left = abt;
        aat.right = act;
        abt.left = adt;
        abt.right = aet;
        act.left = aft;
        act.right = agt;
        resultList = solution.zigzagLevelOrder(aat);
        System.out.println(resultList);

        int[] preorder = new int[]{3, 9, 20, 15, 7};
        int[] inorder = new int[]{9, 3, 15, 20, 7};

//        preorder = new int[]{1, 2};
//        inorder = new int[]{2, 1};
//        TreeNode root = solution.buildTree(preorder, inorder);
//        System.out.println(root);

        inorder = new int[]{9, 3, 15, 20, 7};
        int[] postorder = new int[]{9, 15, 7, 20, 3};
        TreeNode root = solution.buildTree(inorder, postorder);
        System.out.println(root);

        resultList = solution.levelOrderBottom(root);
        System.out.println(resultList);

        ListNode al = new ListNode(-10);
        ListNode bl = new ListNode(-3);
        ListNode cl = new ListNode(0);
        ListNode dl = new ListNode(5);
        ListNode el = new ListNode(9);
        al.next = bl;
        bl.next = cl;
        cl.next = dl;
        dl.next = el;
        TreeNode node = solution.sortedListToBST(al);
        System.out.println(node);

        String roman = solution.intToRoman(1994);
        System.out.println(roman);

        int index = solution.strStr("mississippi", "issipi");
        System.out.println(index);
    }

    // 3、无重复字符的最长子串
    public int lengthOfLongestSubstring(String s) {
        // 哈希集合，记录每个字符是否出现过
        if (s == null || s.length() == 0) {
            return 0;
        }
        int ans = 1;
        char[] chars = s.toCharArray();
        int length = chars.length;
        Set<Character> set = new HashSet<>();
        int right = -1; // 右边界
        for (int left = 0; left < length; left++) {
            if (left > 0) {
                set.remove(chars[left - 1]);
            }
            for (int j = right + 1; j < length; j++) {
                if (!set.contains(chars[j])) {
                    set.add(chars[j]);
                    right = j;
                    ans = Math.max(ans, right - left + 1);
                } else {
                    right = j - 1;
                    ans = Math.max(ans, right - left + 1);
                    break;
                }
            }
//            System.out.println("left=" + left + ",right=" + right + ",ans=" + ans);
        }
        return ans;
    }

    // 1208、尽可能使字符串相等
    public int equalSubstring(String s, String t, int maxCost) {
        char[] sc = s.toCharArray();
        char[] tc = t.toCharArray();
        int length = sc.length;

        int right = -1;
        int res = 0;
        int curCost = 0;
        int maxLen = 0;
        for (int left = 0; left < length; left++) {
            if (left > 0) {
                curCost = curCost - Math.abs(sc[left - 1] - tc[left - 1]);
                res--;
            }
            for (int j = right + 1; j < length; j++) {
                int cost = Math.abs(sc[j] - tc[j]);
                if (cost > 0) {
                    if (curCost + cost <= maxCost) {
                        curCost = curCost + cost;
                        res++;
                        right = j;
                        maxLen = Math.max(maxLen, res);
                    } else {
                        right = j - 1;
                        maxLen = Math.max(maxLen, res);
                        break;
                    }
                } else {
                    curCost = curCost + cost;
                    res++;
                    right = j;
                    maxLen = Math.max(maxLen, res);
                }
            }
        }
        return maxLen;
    }

    // 76、最小覆盖子串
    public String minWindow(String s, String t) {

        return "";
    }

    // 5、最长回文子串 时间复杂度O(n²)
    public String longestPalindrome(String s) {
        // 定义：dp[i][j]值为1，表示s[i:j]为回文串；为2，表示不为回文串。s[i:j]表示索引从i到j的子串，包括i和j。
        // dp[i][j] = (s[i] == s[j] && dp[i+1][j-1] == 1) ? 1 : 2
        int length = s.length();
        int[][] dp = new int[length][length];
        int maxLength = 1;
        String maxLengthStr = s.substring(0, 1);
        for (int j = 0; j < length; j++) {
            for (int i = 0; i <= j; i++) {
                if (j == i) {
                    dp[i][j] = 1;
                } else {
                    boolean b = s.charAt(i) == s.charAt(j);
                    if (j == i + 1 || j == i + 2) {
                        dp[i][j] = b ? 1 : 2;
                    } else {
                        dp[i][j] = b && dp[i + 1][j - 1] == 1 ? 1 : 2;
                    }
                }

                if (dp[i][j] == 1) {
                    if (j - i + 1 > maxLength) {
                        maxLength = j - i + 1;
                        maxLengthStr = s.substring(i, j + 1);
                    }
                }
            }
        }
        return maxLengthStr;
    }

    public ListNode reverseBetween(ListNode head, int left, int right) {
        if (head == null || left >= right) {
            return head;
        }

        ListNode hair = new ListNode(0, head);
        ListNode cur = hair;
        ListNode before = hair;
        int i = 0;
        while (i < left) {
            cur = cur.next;
            i++;
            if (i == left - 1) {
                before = cur;
            }
        }
        ListNode first = cur;

        i = 0;
        ListNode pre = null;
        while (i <= right - left && cur != null) {
            ListNode next = cur.next;
            cur.next = pre;
            pre = cur;
            cur = next;
            i++;
        }
        first.next = cur;
        before.next = pre;
        return hair.next;
    }

    public List<Integer> spiralOrder(int[][] matrix) {
        List<Integer> resultList = new ArrayList<>();
        int minR = 0; // 最小行
        int maxR = matrix.length - 1; // 最大行

        int minC = 0; // 最小列
        int maxC = matrix[0].length - 1; // 最大列

        while (true) {
            if (minC > maxC) {
                return resultList;
            }
            increaseColumn(matrix, minR, minC, maxC, resultList);
            minR++;

            if (minR > maxR) {
                return resultList;
            }
            increaseRow(matrix, maxC, minR, maxR, resultList);
            maxC--;

            if (maxC < minC) {
                return resultList;
            }
            decreaseColumn(matrix, maxR, maxC, minC, resultList);
            maxR--;

            if (maxR < minR) {
                return resultList;
            }
            decreaseRow(matrix, minC, maxR, minR, resultList);
            minC++;
        }
    }

    // 保持行不变，从左往右一列列打印。i是行，startC是开始列，endC是结束列，包括startC和endC，startC<=endC
    private void increaseColumn(int[][] matrix, int i, int startC, int endC, List<Integer> resultList) {
        for (int j = startC; j <= endC; j++) {
//            System.out.println(matrix[i][j]);
            resultList.add(matrix[i][j]);
        }
    }

    // 保持行不变，从右往左一列列打印。i是行，startC是开始列，endC是结束列，包括startC和endC，startC>=endC
    private void decreaseColumn(int[][] matrix, int i, int startC, int endC, List<Integer> resultList) {
        for (int j = startC; j >= endC; j--) {
//            System.out.println(matrix[i][j]);
            resultList.add(matrix[i][j]);
        }
    }

    // 保持列不变，从上往下一行行打印。i是列，startR是开始行，endR是结束行，包括startR和endR，startR<=endR
    private void increaseRow(int[][] matrix, int i, int startR, int endR, List<Integer> resultList) {
        for (int j = startR; j <= endR; j++) {
//            System.out.println(matrix[j][i]);
            resultList.add(matrix[j][i]);
        }
    }

    // 保持列不变，从下往上一行行打印。i是列，startR是开始行，endR是结束行，包括startR和endR，startR>=endR
    private void decreaseRow(int[][] matrix, int i, int startR, int endR, List<Integer> resultList) {
        for (int j = startR; j >= endR; j--) {
//            System.out.println(matrix[j][i]);
            resultList.add(matrix[j][i]);
        }
    }

    public ListNode detectCycle(ListNode head) {
        Set<ListNode> set = new HashSet<>();
        while (head != null) {
            if (set.contains(head)) {
                return head;
            }
            set.add(head);
            head = head.next;
        }
        return null;
    }

    // 143 重排链表
    public void reorderList(ListNode head) {
        ListNode cur = head;
        List<ListNode> list = new ArrayList<>();
        while (cur != null) {
            list.add(cur);
            cur = cur.next;
        }
        for (int i = 0; i < list.size() / 2; i++) {
            ListNode node0 = list.get(i);
            ListNode node1 = list.get(i + 1);
            ListNode nodeL = list.get(list.size() - 1 - i);
            node0.next = nodeL;
            nodeL.next = node1;
            node1.next = null;
        }
    }

    // 199 二叉树的右视图
    public List<Integer> rightSideView(TreeNode root) {
        List<Integer> list = new ArrayList<>();
        if (root == null) {
            return list;
        }

        Queue<TreeNode> queue = new ArrayDeque<>();
        queue.offer(root);
        while (!queue.isEmpty()) {
            int size = queue.size();
            for (int i = 0; i < size; i++) {
                TreeNode n = queue.poll();
                assert n != null;
                if (i == size - 1) {
                    list.add(n.val);
                }
                if (n.left != null) {
                    queue.offer(n.left);
                }
                if (n.right != null) {
                    queue.offer(n.right);
                }
            }
        }
        return list;
    }

    // 给你一个链表，删除链表的倒数第n个结点，并且返回链表的头结点。
    // 19、删除链表的倒数第N个结点
    public ListNode removeNthFromEnd(ListNode head, int n) {
        ListNode hair = new ListNode();
        hair.next = head;

        ListNode quick = hair;
        ListNode slow = hair;

        for (int i = 1; i <= n; i++) {
            quick = quick.next;
        }
        while (quick.next != null) {
            quick = quick.next;
            slow = slow.next;
        }
        slow.next = slow.next.next;
        return hair.next;
    }

    // 56 合并区间
    public int[][] merge(int[][] intervals) {
        if (intervals == null) {
            return null;
        }

        List<Integer[]> list = new ArrayList<>();
        for (int[] interval : intervals) {
            Integer[] a1 = new Integer[]{interval[0], interval[1]};
            if (list.isEmpty()) {
                list.add(a1);
                continue;
            }

            int size = list.size();
            int flag = 0;
            for (int j = 0; j < size; j++) {
                Integer[] a2 = list.get(j);
                if (isOverlapping(a1, a2)) {
                    list.set(j, innerMerge(a1, a2));
                    flag++;
                    break;
                }
            }
            if (flag == 0) {
                list.add(a1);
            }
        }
        int[][] res = new int[list.size()][];
        for (int i = 0; i < res.length; i++) {
            res[i] = new int[]{list.get(i)[0], list.get(i)[1]};
        }
        return res;
    }

    private boolean isOverlapping(Integer[] a1, Integer[] a2) {
        if (a1[1] < a2[0] || a2[1] < a1[0]) {
            return false;
        }
        return true;
    }

    private Integer[] innerMerge(Integer[] a1, Integer[] a2) {
        if (Objects.equals(a1[0], a2[0]) && Objects.equals(a1[1], a2[1])) { // a2=a1
            return a1;
        } else if (a2[0] <= a1[0] && a2[1] >= a1[1]) { // a2包含a1
            return a2;
        } else if (a1[0] <= a2[0] && a1[1] >= a2[1]) { // a1包含a2
            return a1;
        } else if (Objects.equals(a1[1], a2[0]) || (a2[0] > a1[0] && a2[0] < a1[1])) { // a2[0]在a1[0]和a1[1]之间
            return new Integer[]{a1[0], a2[1]};
        } else if (Objects.equals(a2[1], a1[0]) || (a1[0] > a2[0] && a1[0] < a2[1])) { // a1[0]在a2[0]和a2[1]之间
            return new Integer[]{a2[0], a1[1]};
        }
        return null;
    }

    // 98、验证二叉搜索树
    public boolean isValidBST(TreeNode root) {
        return isValidBST(root, Long.MIN_VALUE, Long.MAX_VALUE);
    }

    // 以root结点为根结点的树的所有结点的值是否在(low, high)区间
    private boolean isValidBST(TreeNode root, long low, long high) {
        if (root == null) {
            return true;
        }
        return (root.val > low && root.val < high) && isValidBST(root.left, low, root.val) && isValidBST(root.right, root.val, high);
    }

    // 99、恢复二叉搜素树
    // 给你二叉搜索树的根结点 root ，该树中的 恰好 两个结点的值被错误地交换。请在不改变其结构的情况下，恢复这棵树 。
    public void recoverTree(TreeNode root) {
        // 中序遍历root，把各结点都放到一个集合中
        List<TreeNode> list = inorderTraversal(root);

        // 遍历这个集合，找出非递增的卡点
        TreeNode x = null; // 值大的结点
        TreeNode y = null; // 值小的结点

//        List<List<TreeNode>> tmpList = new ArrayList<>();
//        for (int i = 0; i < list.size() - 1; i++) {
//            if (list.get(i).val > list.get(i + 1).val) {
//                tmpList.add(Arrays.asList(list.get(i), list.get(i + 1)));
//            }
//        }
//
//        if (tmpList.size() == 1) {
//            x = tmpList.get(0).get(0);
//            y = tmpList.get(0).get(1);
//        } else if (tmpList.size() == 2) {
//            x = tmpList.get(0).get(0);
//            y = tmpList.get(1).get(1);
//        }

        // 找x、y可以优化成如下
        for (int i = 0; i < list.size() - 1; i++) {
            if (list.get(i).val > list.get(i + 1).val) {
                y = list.get(i + 1);
                if (x == null) {
                    x = list.get(i);
                }
            }
        }

        // 交换这俩结点的值
        if (x != null && y != null) {
            int tmp = x.val;
            x.val = y.val;
            y.val = tmp;
        }
    }

    // 中序遍历
    public List<TreeNode> inorderTraversal(TreeNode root) {
        List<TreeNode> list = new ArrayList<>();
        if (root != null) {
            inorderTraversalWithList(root, list);
        }
        return list;
    }

    private void inorderTraversalWithList(TreeNode node, List<TreeNode> list) {
        if (node.left != null) {
            inorderTraversalWithList(node.left, list);
        }
        list.add(node);
        if (node.right != null) {
            inorderTraversalWithList(node.right, list);
        }
    }

    public void nextPermutation(int[] nums) { // 原地修改
        int i = 0;
        int j = 0;

        for (int a = nums.length - 1; a > 0; a--) {
            if (nums[a] > nums[a - 1]) {
                i = a - 1;
                j = a;
                break;
            }
        }

        for (int a = nums.length - 1; a >= j; a--) {
            if (nums[a] > nums[i]) {
                swap(nums, a, i);
                break;
            }
        }

        // 逆序
        for (int a = j; a <= (nums.length - 1 + j) / 2; a++) {
            swap(nums, a, nums.length - 1 + j - a);
        }
    }

    private void swap(int[] arr, int i, int j) {
        if (arr[i] == arr[j]) {
            return;
        }

        int a = arr[i];
        arr[i] = arr[j];
        arr[j] = a;
    }

    // 61、旋转链表
    public ListNode rotateRight(ListNode head, int k) {
        if (k == 0 || head == null) {
            return head;
        }

        ListNode hair = new ListNode();
        hair.next = head;

        int len = 0;
        while (head != null) {
            head = head.next;
            len++;
        }

        k = k % len;
        if (k > 0) {
            ListNode quick = hair.next;
            ListNode slow = hair.next;
            for (int i = 0; i < k; i++) {
                quick = quick.next;
            }
            while (quick.next != null) {
                quick = quick.next;
                slow = slow.next;
            }
            ListNode oriNext = slow.next;
            slow.next = null; // 斩断联系
            quick.next = hair.next;// 建立联系
            return oriNext;
        }
        return hair.next;
    }

    // 75、颜色分类
    // 给定一个包含红色、白色和蓝色、共 n 个元素的数组 nums ，原地对它们进行排序，使得相同颜色的元素相邻，并按照红色、白色、蓝色顺序排列。
    // 我们使用整数 0、 1 和 2 分别表示红色、白色和蓝色。
    // 必须在不使用库内置的 sort 函数的情况下解决这个问题。
    public void sortColors(int[] nums) {
        int i = 0;
        int k = 0;
        while (i < nums.length && k < nums.length) {
            if (nums[i] == 0) {
                for (int j = i; j > 0; j--) {
                    swap(nums, j, j - 1);
                }
                i++;
            } else if (nums[i] == 2) {
                for (int j = i; j < nums.length - 1; j++) {
                    swap(nums, j, j + 1);
                }
            } else {
                i++;
            }
            k++;
        }
    }

    // 95、不同的二叉搜索树II
    public List<TreeNode> generateTrees(int n) {
        if (n == 0) {
            return new ArrayList<>();
        }
        return generateTrees(1, n);
    }

    // 返回序列[start, end]可生成的所有二叉排序树。start、end是值，不是索引
    private List<TreeNode> generateTrees(int start, int end) {
        List<TreeNode> allTrees = new ArrayList<>();
        if (start > end) {
            return allTrees;
        }

        if (start == end) {
            allTrees.add(new TreeNode(start));
        } else {
            for (int i = start; i <= end; i++) {
                // i是根结点，根据[start, i-1]调用generateTrees函数计算左子树集合，根据[i+1, end]调用generateTrees函数计算右子树集合
                List<TreeNode> leftTrees = generateTrees(start, i - 1);
                List<TreeNode> rightTrees = generateTrees(i + 1, end);

                if (leftTrees.size() > 0 && rightTrees.size() > 0) {
                    for (TreeNode leftTree : leftTrees) {
                        for (TreeNode rightTree : rightTrees) {
                            allTrees.add(new TreeNode(i, leftTree, rightTree));
                        }
                    }
                } else if (leftTrees.size() > 0) {
                    for (TreeNode leftTree : leftTrees) {
                        TreeNode root = new TreeNode(i);
                        root.left = leftTree;
                        allTrees.add(root);
                    }
                } else if (rightTrees.size() > 0) {
                    for (TreeNode rightTree : rightTrees) {
                        TreeNode root = new TreeNode(i);
                        root.right = rightTree;
                        allTrees.add(root);
                    }
                } else {
                    allTrees.add(new TreeNode(i));
                }
            }
        }
        return allTrees;
    }

    Map<Integer, Integer> bstNumMap = new HashMap<>();

    // 96、不同的二叉搜索树 n个不同的数可构成的BST个数
    public int numTrees(int n) {
        int num = 0;
        if (bstNumMap.containsKey(n)) {
            num = bstNumMap.get(n);
        } else {
            if (n == 0 || n == 1) {
                num = 1;
            } else {
                for (int i = 1; i <= n; i++) {
                    num = num + numTrees(i - 1) * numTrees(n - i);
                }
            }
            bstNumMap.put(n, num);
        }
        return num;
    }

    // 102、二叉树的层序遍历
    public List<List<Integer>> levelOrderTraversal(TreeNode root) {
        List<List<Integer>> resultList = new ArrayList<>();
        if (root == null) {
            return resultList;
        }
        Queue<TreeNode> queue = new ArrayDeque<>();
        queue.offer(root);
        // queue代表每层的结点队列。在每一次while循环中，都是先从队首取结点，然后把这个结点的子结点放到队尾，然后再从队首取下一个结点。。。
        // 这样一边出一边进的模式，当一次while循环执行完后，queue存储的就是下一层的结点了。
        while (queue.size() > 0) {
            List<Integer> list = new ArrayList<>(); // list存放每一层的从左到右的元素的值
            int size = queue.size();
            for (int i = 0; i < size; i++) {
                TreeNode node = queue.poll();
                list.add(node.val);
                // 把子结点按照从左到右的顺序放到queue中。在下次循环中，也是从左到右取
                if (node.left != null) {
                    queue.offer(node.left);
                }
                if (node.right != null) {
                    queue.offer(node.right);
                }
            }
            resultList.add(list);
        }
        return resultList;
    }

    // 103、二叉树的锯齿形层序遍历
    public List<List<Integer>> zigzagLevelOrder(TreeNode root) {
        List<List<Integer>> resultList = new ArrayList<>();
        if (root == null) {
            return resultList;
        }

        Deque<TreeNode> queue = new ArrayDeque<>();
        queue.add(root);
        int level = 0;
        while (queue.size() > 0) {
            level++;
            int size = queue.size();
            Deque<Integer> deque = new ArrayDeque<>();
            for (int i = 0; i < size; i++) {
                TreeNode node = queue.remove();
                if (level % 2 == 1) {
                    deque.add(node.val);
                } else {
                    deque.addFirst(node.val);
                }

                if (node.left != null) {
                    queue.add(node.left);
                }
                if (node.right != null) {
                    queue.add(node.right);
                }
            }
            if (deque.size() > 0) {
//                System.out.println("level= " + level + ", list= " + deque);
                resultList.add(new ArrayList<>(deque));
            }
        }
        return resultList;
    }

    // 105、从前序与中序遍历序列构造二叉树
//    public TreeNode buildTree(int[] preorder, int[] inorder) {
//        return buildTree(preorder, 0, preorder.length - 1, inorder, 0, inorder.length - 1);
//    }

    // preorder的[ps, pe]表示左子树或右子树
    // inorder的[is, ie]表示左子树或右子树
//    private TreeNode buildTree(int[] preorder, int ps, int pe, int[] inorder, int is, int ie) {
//        if (preorder.length == 0 || inorder.length == 0) {
//            return null;
//        }
//        if (pe == ps) {
//            return new TreeNode(preorder[ps]);
//        }
//        // 根结点在中序遍历数组中的位置
//        int ri = is;
//        for (int i = is; i <= ie; i++) {
//            if (inorder[i] == preorder[ps]) {
//                ri = i;
//                break;
//            }
//        }
//
//        int lps = ps + 1; // 前序遍历数组中，左子树的起始位置
//        int lpe = ps + ri - is; // 前序遍历数组中，左子树的终止位置
//
//        int rps = ps + ri - is + 1; // 前序遍历数组中，右子树的起始位置
//        int rpe = pe; // 前序遍历数组中，右子树的终止位置
//
//        int lis = is; // 中序遍历数组中，左子树的起始位置
//        int lie = ri - 1; // 中序遍历数组中，左子树的终止位置
//
//        int ris = ri + 1; // 中序遍历数组中，右子树的起始位置
//        int rie = ie; // 中序遍历数组中，右子树的终止位置
//
//        TreeNode left = null;
//        if (lpe >= lps && lie >= lis) {
//            left = buildTree(preorder, lps, lpe, inorder, lis, lie);
//        }
//
//        TreeNode right = null;
//        if (rpe >= rps && rie >= ris) {
//            right = buildTree(preorder, rps, rpe, inorder, ris, rie);
//        }
//
//        return new TreeNode(preorder[ps], left, right);
//    }

    public TreeNode buildTree(int[] inorder, int[] postorder) {
        return buildTree(inorder, 0, inorder.length - 1, postorder, 0, postorder.length - 1);
    }

    // 包括两个边界
    private TreeNode buildTree(int[] inorder, int is, int ie, int[] postorder, int ps, int pe) {
        if (inorder == null || is > ie) {
            return null;
        }

        if (postorder == null || ps > pe) {
            return null;
        }

        int rootValue = postorder[pe];

        int ri = is; //rootValue的index
        // 在中序遍历的数组中找到根结点的位置
        for (int i = is; i <= ie; i++) {
            if (inorder[i] == rootValue) {
                ri = i;
                break;
            }
        }
//        System.out.println(ri);

        // 左子树有几个结点    ri-is      在中序数组中位置是[is, ri-1]，在后序数组中位置是[ps, ri-is-1+ps]
        // 右子树有几个结点    ie-ri      在中序数组中位置是[ri+1, ie]，在后续数组中位置是[ri-is+ps, pe-1]

        // 左子结点
        TreeNode left = buildTree(inorder, is, ri - 1, postorder, ps, ri - is - 1 + ps);
        // 右子结点
        TreeNode right = buildTree(inorder, ri + 1, ie, postorder, ri - is + ps, pe - 1);

        return new TreeNode(rootValue, left, right);
    }

    public List<List<Integer>> levelOrderBottom(TreeNode root) {
        Deque<List<Integer>> stack = new ArrayDeque<>();
        if (root == null) {
            return new ArrayList<>(stack);
        }

        Queue<TreeNode> queue = new ArrayDeque<>();
        queue.add(root);

        do {
            int size = queue.size();
            List<Integer> list = new ArrayList<>();
            for (int i = 1; i <= size; i++) {
                TreeNode node = queue.remove();
                list.add(node.val);
                if (node.left != null) {
                    queue.add(node.left);
                }
                if (node.right != null) {
                    queue.add(node.right);
                }
            }
            stack.push(list);
        } while (!queue.isEmpty());
        return new ArrayList<>(stack);
    }

    ListNode globalHead;

    public TreeNode sortedListToBST(ListNode head) {
        globalHead = head;
        int length = getLength(head);
        return buildTree(0, length - 1);
    }

    public int getLength(ListNode head) {
        int ret = 0;
        while (head != null) {
            ++ret;
            head = head.next;
        }
        return ret;
    }

    public TreeNode buildTree(int left, int right) {
        if (left > right) {
            return null;
        }
        int mid = (left + right + 1) / 2;
        TreeNode root = new TreeNode();
        root.left = buildTree(left, mid - 1);

        // TODO 这里是重点，给globalHead重新赋值。想不通
        root.val = globalHead.val;
        globalHead = globalHead.next;

        root.right = buildTree(mid + 1, right);
        return root;
    }

    // 12、整数转罗马数字
    public String intToRoman(int num) {
        String i = "I"; // 1
        String v = "V"; // 5
        String x = "X"; // 10
        String l = "L"; // 50
        String c = "C"; // 100
        String d = "D"; // 500
        String m = "M"; // 1000
        StringBuilder sb = new StringBuilder();
        if (num < 5) {// [1, 5)
            if (num == 4) {
                sb.append(i).append(v);
            } else {
                for (int j = 0; j < num; j++) {
                    sb.append(i);
                }
            }
        } else if (num < 10) { //[5, 10)
            if (num == 9) {
                sb.append(i).append(x);
            } else {
                sb.append(v);
                sb.append(intToRoman(num - 5));
            }
        } else if (num < 50) { //[10, 50)
            if (num < 40) {
                int a = num / 10;
                int b = num - a * 10;
                for (int j = 0; j < a; j++) {
                    sb.append(x);
                }
                sb.append(intToRoman(b));
            } else {
                sb.append(x).append(l);
                sb.append(intToRoman(num - 40));
            }
        } else if (num < 100) { //[50, 100)
            if (num < 90) {
                sb.append(l);
                sb.append(intToRoman(num - 50));
            } else {
                sb.append(x).append(c);
                sb.append(intToRoman(num - 90));
            }
        } else if (num < 500) { //[100, 500)
            if (num < 400) {
                int a = num / 100;
                int b = num - a * 100;
                for (int j = 0; j < a; j++) {
                    sb.append(c);
                }
                sb.append(intToRoman(b));
            } else {
                sb.append(c).append(d);
                sb.append(intToRoman(num - 400));
            }
        } else if (num < 1000) { //[500, 1000)
            if (num < 900) {
                sb.append(d);
                sb.append(intToRoman(num - 500));
            } else {
                sb.append(c).append(m);
                sb.append(intToRoman(num - 900));
            }
        } else if (num < 4000) { //[1000, 4000)
            int a = num / 1000;
            int b = num - a * 1000;
            for (int j = 0; j < a; j++) {
                sb.append(m);
            }
            sb.append(intToRoman(b));
        }
        return sb.toString();
    }

    // 力扣28、找出字符串中第一个匹配项的下标
    public int strStr(String haystack, String needle) {
        char[] chars1 = haystack.toCharArray();
        char[] chars2 = needle.toCharArray();
        if (chars1.length < chars2.length) {
            return -1;
        }

        int left = 0;
        int right = 0;
        for (int i = 0; i < chars1.length; i++) {
            if (chars1[i] != chars2[0]) {
                continue;
            }

            left = i;
            for (int j = 0; j < chars2.length && j + left < chars1.length; j++) {
                if (chars1[j + left] == chars2[j]) {
                    right = j + left;
                } else {
                    break;
                }
            }
            if (right - left + 1 == chars2.length) {
                return left;
            }
        }
        return -1;
    }
}
