package com.kou.leetcodedemo.learn;

import java.util.Arrays;

public class Util {
    public static void swap(int[] arr, int i, int j) {
        if (i != j) {
            arr[i] = arr[i] ^ arr[j];
            arr[j] = arr[i] ^ arr[j];
            arr[i] = arr[i] ^ arr[j];
        }
    }

    public static int[] generateArr(int[] arr) {
        int[] newArr;
        if (arr != null) {
            int length = arr.length;
            newArr = new int[length];
            System.arraycopy(arr, 0, newArr, 0, length);
        } else {
            int length = 8;
            newArr = new int[length];
            for (int i = 0; i < length; i++) {
                newArr[i] = (int) (20 * (Math.random()));
            }
        }
        return newArr;
    }

    // arr1是未排序的数组，arr2是按照自定义算法排序后的数组
    public static void check(int[] arr1, int[] arr2) {
        Arrays.sort(arr1);
        if (Arrays.toString(arr1).equals(Arrays.toString(arr2))) {
//            System.out.println("排序正确");
        } else {
            System.out.println("排序错误");
        }
    }
}
