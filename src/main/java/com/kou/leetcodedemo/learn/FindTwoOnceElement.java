package com.kou.leetcodedemo.learn;

public class FindTwoOnceElement {
    public static void main(String[] args) {
        int[] arr = new int[6];
        arr[0] = 533;
        arr[1] = 744;
        arr[2] = 445;
        arr[3] = 344;
        arr[4] = 744;
        arr[5] = 344;
        printOddTimesNum2(arr);
    }

    public static void printOddTimesNum2(int[] arr) {
        int eor = 0;
        for (int f : arr) {
            eor = eor ^ f;
        }
        System.out.println("eor= " + eor);

        int rightOne = eor & (~eor + 1);//
        System.out.println("rightOne= " + rightOne);

        int onlyOne = 0;
        for (int f : arr) {
            if ((f & rightOne) == 1) {
                onlyOne = onlyOne ^ f;
            }
        }
        System.out.println(onlyOne + "," + (eor ^ onlyOne));
    }

}


