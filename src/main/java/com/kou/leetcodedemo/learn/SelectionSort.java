package com.kou.leetcodedemo.learn;

import java.util.Arrays;

public class SelectionSort {
    // 选择排序。每次都挑出最小的
    public static void main(String[] args) {
        int[] arr = Util.generateArr(null);
        System.out.println(Arrays.toString(arr));
        sort(arr);
        System.out.println(Arrays.toString(arr));
    }

    public static void sort(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            int minIndex = i;
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[minIndex] > arr[j]) {
                    minIndex = j;
                }
            }
            Util.swap(arr, i, minIndex);
        }
    }

}
