package com.kou.leetcodedemo.learn;

public class InsertSort {

    public static void main(String[] args) {

        for (int i = 0; i < 10000; i++) {
            int[] arr = Util.generateArr(null);
            int[] oriArr = Util.generateArr(arr);
            sort(arr);
            Util.check(oriArr, arr);
        }
    }

    public static void sort(int[] arr) {
        if (arr == null || arr.length == 1) {
            return;
        }

        for (int i = 0; i < arr.length; i++) {
            for (int j = arr.length - 2; j >= 0; j--) {//从倒数第二个开始，往左
                if (arr[j + 1] < arr[j]) {// 右边小于左边
                    Util.swap(arr, j + 1, j);
                }
            }
        }
    }

}
